package foo

type Result[T any] struct {
	Code string
	Msg  string
	Data T
}

func FailureResult(code string, msg string) Result[any] {
	return Result[any]{Code: code, Msg: msg}
}

func SuccessResult[T any](data T) Result[T] {
	return Result[T]{Code: "SUCCESS", Msg: "成功", Data: data}
}